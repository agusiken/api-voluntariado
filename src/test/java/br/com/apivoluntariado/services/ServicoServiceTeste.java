package br.com.apivoluntariado.services;

import br.com.apivoluntariado.models.Pessoa;
import br.com.apivoluntariado.models.Servico;
import br.com.apivoluntariado.repositories.ServicoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class ServicoServiceTeste {

    @MockBean
    ServicoRepository servicoRepository;

    @Autowired
    ServicoService servicoService;

    private Pessoa pessoa;
    private Servico servico;

    @BeforeEach
    public void setup() {
        pessoa = new Pessoa();
        pessoa.setNome("Giovanni");
        pessoa.setEmail("gvn.gentile@gmail.com");
        pessoa.setTelefone("982633881");
        pessoa.setLogradouro("Rua XPTO, 155");
        pessoa.setBairro("Mooca");
        pessoa.setCidade("São Paulo");

        servico = new Servico();
        servico.setTipoServico("Doação de alimentos");

        List<Pessoa> pessoas = new ArrayList<>();
        pessoas.add(pessoa);

//        servico.setPessoas(pessoas);

        List<Servico> servicos = new ArrayList<>();
        servicos.add(servico);

    }

    @Test
    public void testarSalvarServico() {

        Mockito.when(servicoRepository.save(servico)).thenReturn(servico);
        Servico servicoTeste = servicoService.salvarServico(servico);

        Assertions.assertSame(servico, servicoTeste);

    }



    @Test
    public void testarConsultarServicosPorTipoServico() {
        Iterable<Servico> servicos = Arrays.asList(servico);
        Mockito.when(servicoRepository.findByTipoServicoContains(servico.getTipoServico())).thenReturn(servicos);

        Iterable<Servico> servicosTest = servicoService.consultarServicosTodos(servico.getTipoServico());

        Assertions.assertEquals(servicos, servicosTest);
    }


    @Test
    public void testarConsultarServicosTodos() {
        Iterable<Servico> servicos = Arrays.asList(servico);
        Mockito.when(servicoRepository.findAll()).thenReturn(servicos);

        Iterable<Servico> servicosTest = servicoService.consultarServicosTodos();

        Assertions.assertEquals(servicos, servicosTest);
    }


}
