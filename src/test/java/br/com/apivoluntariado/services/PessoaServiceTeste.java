package br.com.apivoluntariado.services;

import br.com.apivoluntariado.enums.TipoPessoaEnum;
import br.com.apivoluntariado.models.Pessoa;
import br.com.apivoluntariado.models.Servico;
import br.com.apivoluntariado.repositories.PessoaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class PessoaServiceTeste {

    @MockBean
    PessoaRepository pessoaRepository;

    @Autowired
    PessoaService pessoaService;

    private Pessoa pessoa;
    private Servico servico;

    @BeforeEach
    public void setup() {
        pessoa = new Pessoa();
        pessoa.setNome("Giovanni");
        pessoa.setEmail("gvn.gentile@gmail.com");
        pessoa.setTelefone("982633881");
        pessoa.setLogradouro("Rua XPTO, 155");
        pessoa.setBairro("Mooca");
        pessoa.setCidade("São Paulo");

        servico = new Servico();
        servico.setTipoServico("Doação de alimentos");

        List<Pessoa> pessoas = new ArrayList<>();
        pessoas.add(pessoa);

//        servico.setPessoas(pessoas);

        List<Servico> servicos = new ArrayList<>();
        servicos.add(servico);

    }

    @Test
    public void testarSalvarPessoaVoluntario() {
        pessoa.setTipoPessoa(TipoPessoaEnum.VOLUNTARIO);

        Mockito.when(pessoaRepository.save(pessoa)).thenReturn(pessoa);
        Pessoa pessoaTest = pessoaService.salvarPessoa(pessoa);

        Assertions.assertSame(pessoa, pessoaTest);

    }

    @Test
    public void testarSalvarPessoaSolicitante() {
        pessoa.setTipoPessoa(TipoPessoaEnum.SOLICITANTE);

        Mockito.when(pessoaRepository.save(pessoa)).thenReturn(pessoa);
        Pessoa pessoaTest = pessoaService.salvarPessoa(pessoa);

        Assertions.assertSame(pessoa, pessoaTest);

    }

    @Test
    public void testarConsultarVoluntariosTodos() {
        Iterable<Pessoa> voluntarios = Arrays.asList(pessoa);
        Mockito.when(pessoaRepository.findAllByTipoPessoa(TipoPessoaEnum.VOLUNTARIO)).thenReturn(voluntarios);

        Iterable<Pessoa> voluntariosTest = pessoaService.consultarVoluntariosTodos();

        Assertions.assertEquals(voluntarios, voluntariosTest);
    }

    @Test
    public void testarConsultarVoluntariosBairro() {
        Iterable<Pessoa> voluntarios = Arrays.asList(pessoa);
        Mockito.when(pessoaRepository.findAllByTipoPessoaAndBairroContains(TipoPessoaEnum.VOLUNTARIO,
                     pessoa.getBairro())).thenReturn(voluntarios);

        Iterable<Pessoa> voluntariosTest = pessoaService.consultarVoluntariosBairro(pessoa.getBairro());

        Assertions.assertEquals(voluntarios, voluntariosTest);

    }

    @Test
    public void testarConsultarVoluntariosCidade() {
        Iterable<Pessoa> voluntarios = Arrays.asList(pessoa);
        Mockito.when(pessoaRepository.findAllByTipoPessoaAndCidadeContains(TipoPessoaEnum.VOLUNTARIO,
                pessoa.getCidade())).thenReturn(voluntarios);

        Iterable<Pessoa> voluntariosTest = pessoaService.consultarVoluntariosCidade(pessoa.getCidade());

        Assertions.assertEquals(voluntarios, voluntariosTest);

    }

}
