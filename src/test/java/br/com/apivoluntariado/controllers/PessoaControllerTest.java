package br.com.apivoluntariado.controllers;


import br.com.apivoluntariado.enums.TipoPessoaEnum;
import br.com.apivoluntariado.models.Pessoa;
import br.com.apivoluntariado.models.Servico;
import br.com.apivoluntariado.security.JWTUtil;
import br.com.apivoluntariado.services.LoginUsuarioService;
import br.com.apivoluntariado.services.PessoaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(PessoaController.class)
@Import(JWTUtil.class)
public class PessoaControllerTest {

    @MockBean
    private PessoaService pessoaService;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    @Autowired
    private MockMvc mockMvc;

    Pessoa pessoa;
    Servico servico;

    List<Servico> servicos;

    List<Pessoa> pessoaList;

    @BeforeEach
    public void setUp() {
        pessoa = new Pessoa();
        pessoa.setId((long)1);
        pessoa.setNome("Giovanni");
        pessoa.setEmail("gvn.gentile@gmail.com");
        pessoa.setTelefone("982633881");
        pessoa.setLogradouro("Rua XPTO, 155");
        pessoa.setBairro("Mooca");
        pessoa.setCidade("São Paulo");

        servico = new Servico();
        servico.setTipoServico("Doação de alimentos");

        pessoaList = new ArrayList<>();
        pessoaList.add(pessoa);

        servico.setId((long)1);
//        servico.setPessoas(pessoaList);

        servicos = new ArrayList<>();
        servicos.add(servico);
    }

    @Test
    @WithMockUser(username = "gvn.gentile@gmail.com", password = "123teste")
    public void testarCadastrarPessoaVoluntario() throws Exception {

        pessoa.setTipoPessoa(TipoPessoaEnum.VOLUNTARIO);
        Mockito.when(pessoaService.salvarPessoa(Mockito.any(Pessoa.class))).then(pessoa -> {
            this.pessoa.setId((long) 1);
            return this.pessoa;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePessoa = mapper.writeValueAsString(pessoa);

        mockMvc.perform(MockMvcRequestBuilders.post("/pessoas")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePessoa))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1))
                );
    }

    @Test
    @WithMockUser(username = "gvn.gentile@gmail.com", password = "123teste")
    public void testarCadastrarPessoaSolicitante() throws Exception {

        pessoa.setTipoPessoa(TipoPessoaEnum.SOLICITANTE);
        Mockito.when(pessoaService.salvarPessoa(Mockito.any(Pessoa.class))).then(pessoa -> {
            this.pessoa.setId((long) 1);
            return this.pessoa;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePessoa = mapper.writeValueAsString(pessoa);

        mockMvc.perform(MockMvcRequestBuilders.post("/pessoas")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePessoa))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1))
                );
    }


    @Test
    @WithMockUser(username = "gvn.gentile@gmail.com", password = "123teste")
    public void testarConsultarVoluntarioPorParametro() throws Exception{

        pessoa.setTipoPessoa(TipoPessoaEnum.VOLUNTARIO);
        Mockito.when(pessoaService.consultarVoluntariosBairro(Mockito.anyString())).then(pessoas -> {

            Iterable<Pessoa> pessoasRetorno = pessoaList;
            return pessoasRetorno;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/pessoas?bairro=Teste"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }


}
