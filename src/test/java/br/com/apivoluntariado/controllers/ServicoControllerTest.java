package br.com.apivoluntariado.controllers;

import br.com.apivoluntariado.dto.RespostaDTOServico;
import br.com.apivoluntariado.enums.TipoPessoaEnum;
import br.com.apivoluntariado.models.Pessoa;
import br.com.apivoluntariado.models.Servico;
import br.com.apivoluntariado.security.JWTUtil;
import br.com.apivoluntariado.services.LoginUsuarioService;
import br.com.apivoluntariado.services.ServicoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;


@WebMvcTest(ServicoController.class)
@Import(JWTUtil.class)
public class ServicoControllerTest {

    @MockBean
    private ServicoService servicoService;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    @Autowired
    private MockMvc mockMvc;

    Pessoa pessoa;
    Servico servico;
    List<Pessoa> pessoas;
    List<Servico> servicos;


    @BeforeEach
    public void setUp() {
        servico = new Servico();
        servico.setTipoServico("Alimentos");
        servico.setId((long) 1);

        pessoa = new Pessoa();
        pessoa.setId((long) 1);
        pessoa.setNome("Giovanni");
        pessoa.setEmail("gvn.gentile@gmail.com");
        pessoa.setTelefone("982633881");
        pessoa.setLogradouro("Rua XPTO, 155");
        pessoa.setBairro("Mooca");
        pessoa.setCidade("São Paulo");

        pessoas = new ArrayList<>();
        pessoas.add(pessoa);

//        servico.setPessoas(pessoas);

        servicos = new ArrayList<>();
        servicos.add(servico);
    }

    @Test
    @WithMockUser(username = "gvn.gentile@gmail.com", password = "123teste")
    public void testarCadastrarServico() throws Exception {

        pessoa.setTipoPessoa(TipoPessoaEnum.VOLUNTARIO);

        Mockito.when(servicoService.salvarServico(Mockito.any(Servico.class))).then(servicoObjeto -> {
            servico.setId((long) 1);
            return servico;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeServico = mapper.writeValueAsString(servico);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/servicos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeServico))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.tipoServico", CoreMatchers.equalTo("Alimentos")));

        MvcResult mvcResult = resultActions.andReturn();
        String jsonResposta = mvcResult.getResponse().getContentAsString();
        RespostaDTOServico respostaDTOServico = mapper.readValue(jsonResposta, RespostaDTOServico.class);
    }

    @Test
    @WithMockUser(username = "gvn.gentile@gmail.com", password = "123teste")
    public void testarConsultarServicos() throws Exception {

        pessoa.setTipoPessoa(TipoPessoaEnum.VOLUNTARIO);

        Mockito.when(servicoService.consultarServicosTodos(Mockito.anyString())).then(servicosObjeto -> {
            Iterable<Servico> servicosRetorno = servicos;
            return servicosRetorno;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/servicos?tipoServico=Alimentos"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));

    }

}
