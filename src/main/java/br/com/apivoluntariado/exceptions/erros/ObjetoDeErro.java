package br.com.apivoluntariado.exceptions.erros;

public class ObjetoDeErro {

    private String valorRejeitado;
    private String mensagemDeErro;

    public ObjetoDeErro(){

    }

    public ObjetoDeErro(String valorRejeitado, String mensagemDeErro) {
        this.valorRejeitado = valorRejeitado;
        this.mensagemDeErro = mensagemDeErro;
    }

    public String getValorRejeitado() {
        return valorRejeitado;
    }

    public void setValorRejeitado(String valorRejeitado) {
        this.valorRejeitado = valorRejeitado;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }
}
