package br.com.apivoluntariado.services;

import br.com.apivoluntariado.models.Servico;
import br.com.apivoluntariado.repositories.ServicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicoService {

    @Autowired
    private ServicoRepository servicoRepository;

    public Servico salvarServico(Servico servico) {
        Servico servicoObjeto = servicoRepository.save(servico);
        return servicoObjeto;
    }

    public Iterable<Servico> consultarServicosTodos() {
        Iterable<Servico> servicos = servicoRepository.findAll();
        return servicos;
    }

    public Iterable<Servico> consultarServicosTodos(String tipoServico) {
        Iterable<Servico> servicos = servicoRepository.findByTipoServicoContains(tipoServico);
        return servicos;
    }

    public Servico consultarServicosPorId(Long id){
        Optional<Servico> optionalServico = servicoRepository.findById(id);

        if(optionalServico.isPresent()){
            return optionalServico.get();
        }
        throw new RuntimeException("Servico Nao Encontrado");
    }

    public List<Servico> consultarServicosTodosPorId(List<Long> id){
        Iterable<Servico> servicos = servicoRepository.findAllById(id);

        return (List) servicos;
    }



}
