package br.com.apivoluntariado.services;

import br.com.apivoluntariado.models.Usuario;
import br.com.apivoluntariado.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

        @Autowired
        private UsuarioRepository usuarioRepository;

        @Autowired
        private BCryptPasswordEncoder bCryptPasswordEncoder;

        public Usuario salvarUsuario(Usuario usuario){
            String encoder = bCryptPasswordEncoder.encode(usuario.getSenha());
            usuario.setSenha(encoder);

            Usuario usuarioObjeto = usuarioRepository.save(usuario);
            return usuarioObjeto;
        }
}
