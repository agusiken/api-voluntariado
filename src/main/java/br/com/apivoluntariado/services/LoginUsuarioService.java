package br.com.apivoluntariado.services;

import br.com.apivoluntariado.models.Usuario;
import br.com.apivoluntariado.repositories.UsuarioRepository;
import br.com.apivoluntariado.security.LoginUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LoginUsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByEmail(email);
        if(usuario == null){
            throw new UsernameNotFoundException(email);
        }
        LoginUsuario loginUsuario =
                new LoginUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        return loginUsuario;
    }
}
