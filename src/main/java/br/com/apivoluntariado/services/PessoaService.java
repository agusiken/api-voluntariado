package br.com.apivoluntariado.services;

import br.com.apivoluntariado.enums.TipoPessoaEnum;
import br.com.apivoluntariado.models.Pessoa;
import br.com.apivoluntariado.models.Servico;
import br.com.apivoluntariado.repositories.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private ServicoService servicoService;


    public Pessoa salvarPessoa(Pessoa pessoa) {
        List<Long> idServico = new ArrayList<>();

        if(pessoa.getTipoPessoa().equals(TipoPessoaEnum.VOLUNTARIO)) {

            for (Servico servico : pessoa.getServicos()) {
                Long id = servico.getId();
                idServico.add(id);
            }
            List<Servico> servicos = servicoService.consultarServicosTodosPorId(idServico);
            pessoa.setServicos(servicos);
        }

        Pessoa pessoaObjeto = pessoaRepository.save(pessoa);
        return pessoaObjeto;
    }

    public Iterable<Pessoa> consultarVoluntariosTodos() {
        Iterable<Pessoa> pessoas = pessoaRepository.findAllByTipoPessoa(TipoPessoaEnum.VOLUNTARIO);
        return pessoas;
    }

    public Iterable<Pessoa> consultarVoluntariosBairro(String bairro) {
        Iterable<Pessoa> pessoas = pessoaRepository.findAllByTipoPessoaAndBairroContains(TipoPessoaEnum.VOLUNTARIO, bairro);
        return pessoas;
    }

    public Iterable<Pessoa> consultarVoluntariosCidade(String cidade) {
        Iterable<Pessoa> pessoas = pessoaRepository.findAllByTipoPessoaAndCidadeContains(TipoPessoaEnum.VOLUNTARIO, cidade);
        return pessoas;
    }

    public Iterable<Pessoa> consultarVoluntariosServico(String tipoServico) {
        Iterable<Servico> servicos = servicoService.consultarServicosTodos(tipoServico);

        List<Servico> servicoList = new ArrayList<>();

        servicos.forEach(servicoList::add);

        Servico servico = servicoList.get(0);

        Iterable<Pessoa> pessoas = pessoaRepository.findAllByTipoPessoaAndServicos(TipoPessoaEnum.VOLUNTARIO, servico);

        return pessoas;
    }

    public Iterable<Pessoa> consultarVoluntariosServico(Long id) {
        Servico servico = servicoService.consultarServicosPorId(id);

        Iterable<Pessoa> pessoas = pessoaRepository.findAllByTipoPessoaAndServicos(TipoPessoaEnum.VOLUNTARIO, servico);

        return pessoas;
    }
}
