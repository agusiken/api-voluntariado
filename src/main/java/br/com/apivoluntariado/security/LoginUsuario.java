package br.com.apivoluntariado.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class LoginUsuario  implements UserDetails {
    private Long id;
    private String email;
    private String senha;

    public LoginUsuario() {
    }

    public LoginUsuario(Long id, java.lang.String email, java.lang.String senha) {
        this.id = id;
        this.email = email;
        this.senha = senha;
    }


    @java.lang.Override
    public java.util.Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @java.lang.Override
    public java.lang.String getPassword() {
        return this.senha;
    }

    @java.lang.Override
    public java.lang.String getUsername() {
        return this.email;
    }

    @java.lang.Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @java.lang.Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @java.lang.Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @java.lang.Override
    public boolean isEnabled() {
        return true;
    }
}
