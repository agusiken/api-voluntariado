package br.com.apivoluntariado.security;

import br.com.apivoluntariado.services.LoginUsuarioService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroDeAutorizacao extends BasicAuthenticationFilter {

    private JWTUtil jwtUtil;

    private LoginUsuarioService loginUsuarioService;

    public FiltroDeAutorizacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil,
                               LoginUsuarioService loginUsuarioService) {
        super(authenticationManager);

        this.jwtUtil = jwtUtil;
        this.loginUsuarioService = loginUsuarioService;

    }

    private UsernamePasswordAuthenticationToken getAutenticacao(HttpServletRequest request, String token){
        if (jwtUtil.tokenValido(token)){
            String username = jwtUtil.getUsername(token);
            UserDetails usuario = loginUsuarioService.loadUserByUsername(username);
            return new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
        }
        return null;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String authorizationHeader = request.getHeader("Authorization");

        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            String tokenlimpo = authorizationHeader.substring(7);
            UsernamePasswordAuthenticationToken autenticacao = getAutenticacao(request, tokenlimpo);

            if(autenticacao !=null){
                SecurityContextHolder.getContext();
            }
        }

        chain.doFilter(request, response);
    }

}
