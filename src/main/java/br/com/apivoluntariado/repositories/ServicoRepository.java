package br.com.apivoluntariado.repositories;


import br.com.apivoluntariado.models.Servico;
import org.springframework.data.repository.CrudRepository;

public interface ServicoRepository extends CrudRepository<Servico, Long> {

    Iterable<Servico> findByTipoServicoContains(String tipoServico);
}
