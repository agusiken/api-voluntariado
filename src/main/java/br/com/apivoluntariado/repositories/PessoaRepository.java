package br.com.apivoluntariado.repositories;

import br.com.apivoluntariado.enums.TipoPessoaEnum;
import br.com.apivoluntariado.models.Pessoa;
import br.com.apivoluntariado.models.Servico;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PessoaRepository extends CrudRepository<Pessoa, Long> {

    Iterable<Pessoa> findAllByTipoPessoa(TipoPessoaEnum tipoPessoaEnum);
    Iterable<Pessoa> findAllByTipoPessoaAndBairroContains(TipoPessoaEnum tipoPessoaEnum, String bairro);
    Iterable<Pessoa> findAllByTipoPessoaAndCidadeContains(TipoPessoaEnum tipoPessoaEnum, String cidade);

    Iterable<Pessoa> findAllByTipoPessoaAndServicos(TipoPessoaEnum tipoPessoaEnum, Servico servico);

}
