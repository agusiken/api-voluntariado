package br.com.apivoluntariado.controllers;

import br.com.apivoluntariado.dto.RespostaDTOServico;
import br.com.apivoluntariado.models.Pessoa;
import br.com.apivoluntariado.models.Servico;
import br.com.apivoluntariado.services.ServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/servicos")
public class ServicoController {

    @Autowired
    private ServicoService servicoService;

    @PostMapping
    public ResponseEntity<RespostaDTOServico> cadastrarServico(@RequestBody Servico servico){
        Servico servicoObjeto = servicoService.salvarServico(servico);

        RespostaDTOServico respostaDTOServico = new RespostaDTOServico();
        respostaDTOServico.setTipoServico(servicoObjeto.getTipoServico());

        return ResponseEntity.status(201).body(respostaDTOServico);
    }


    @GetMapping
    public Iterable<Servico> consultarServicos(@RequestParam(name = "tipoServico", required = false) String tipoServico){
        if (tipoServico != null){
            Iterable<Servico> servicosObjeto = servicoService.consultarServicosTodos(tipoServico);
            return servicosObjeto;
        }

        Iterable<Servico> servicosObjeto = servicoService.consultarServicosTodos();
        return servicosObjeto;
    }

    @GetMapping("/{id}")
    public Servico consultarServicosPorId(@PathVariable Long id){
        Servico servicosObjeto = servicoService.consultarServicosPorId(id);
        return servicosObjeto;

    }

}
