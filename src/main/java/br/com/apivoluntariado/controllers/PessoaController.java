package br.com.apivoluntariado.controllers;

import br.com.apivoluntariado.models.Pessoa;
import br.com.apivoluntariado.services.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pessoas")
public class PessoaController {

    @Autowired
    private PessoaService pessoaService;

    @PostMapping
    public ResponseEntity<Pessoa> cadastrarPessoa(@RequestBody @Valid Pessoa pessoa){
        Pessoa pessoaObjeto = pessoaService.salvarPessoa(pessoa);
        return ResponseEntity.status(201).body(pessoaObjeto);
    }

    @GetMapping
    public Iterable<Pessoa> consultarVoluntarios(@RequestParam(name = "bairro", required = false) String bairro,
                                                 @RequestParam(name = "cidade", required = false) String cidade,
                                                 @RequestParam(name = "tipoServico", required=false) String tipoServico){
        if (bairro != null){
            Iterable<Pessoa> pessoaObjeto = pessoaService.consultarVoluntariosBairro(bairro);
            return pessoaObjeto;
        }

        if (cidade != null) {
            Iterable<Pessoa> pessoaObjeto = pessoaService.consultarVoluntariosCidade(cidade);
            return pessoaObjeto;
        }

        if (tipoServico != null){
            Iterable<Pessoa> pessoaObjeto = pessoaService.consultarVoluntariosServico(tipoServico);
            return pessoaObjeto;
        }

        Iterable<Pessoa> pessoaObjeto = pessoaService.consultarVoluntariosTodos();
        return pessoaObjeto;
    }

    @GetMapping("/servico/{id}")
    public Iterable<Pessoa> consultarVoluntariosServico(@PathVariable Long id){
        Iterable<Pessoa> pessoaObjeto = pessoaService.consultarVoluntariosServico(id);
        return pessoaObjeto;
    }

}
