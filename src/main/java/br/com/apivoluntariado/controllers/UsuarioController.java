package br.com.apivoluntariado.controllers;

import br.com.apivoluntariado.models.Usuario;
import br.com.apivoluntariado.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario registrarUSuario(@RequestBody @Valid Usuario usuario){
        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);
        return usuarioObjeto;
    }
}
