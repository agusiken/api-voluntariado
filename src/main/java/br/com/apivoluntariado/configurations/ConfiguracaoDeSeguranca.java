package br.com.apivoluntariado.configurations;

import br.com.apivoluntariado.security.JWTUtil;
import br.com.apivoluntariado.services.LoginUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class ConfiguracaoDeSeguranca extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private LoginUsuarioService loginUsuarioService;

    private static final String[] PUBLIC_MATCHERS_GET = {
            "/pessoas",
            "/pessoas**",
            "/pessoas/**",
            "/servicos",
            "/servicos**",
            "/servicos/**"
    };

    private static final String[] PUBLIC_MATCHERS_POST = {
            "/pessoas",
            "/pessoas**",
            "/pessoas/**",
            "/servicos",
            "/servicos**",
            "/servicos/**"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors();

        http.authorizeRequests().antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
                .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll();

        super.configure(http);
    }
}
