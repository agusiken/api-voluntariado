package br.com.apivoluntariado.dto;

public class RespostaDTOServico {
    private String tipoServico;

    public RespostaDTOServico() {
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }
}
