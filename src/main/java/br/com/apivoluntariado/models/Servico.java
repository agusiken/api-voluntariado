package br.com.apivoluntariado.models;

import br.com.apivoluntariado.enums.TipoPessoaEnum;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "servico" )
public class Servico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "tipo_servico", length = 100, nullable = false)
    private String tipoServico;

//    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "servicos")
//    private List<Pessoa> pessoas;

    public Servico() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

//    public List<Pessoa> getPessoas() {
//        return pessoas;
//    }
//
//    public void setPessoas(List<Pessoa> pessoas) {
//        this.pessoas = pessoas;
//    }
}
