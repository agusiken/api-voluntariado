package br.com.apivoluntariado.models;


import br.com.apivoluntariado.enums.TipoPessoaEnum;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Pessoa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private TipoPessoaEnum tipoPessoa;

    @Size(min = 5, max = 100, message = "O nome deve ter entre 5 à 10 caracteres")
    @NotBlank(message = "O nome não pode ser vazio")
    @NotNull(message = "O campo nome não pode ser nulo")
    private String nome;

    @Email(message = "Formato do email inválido")
    @NotNull(message = "O campo email não pode ser nulo")
    private String email;

    @NotNull(message = "O campo telefone não pode ser nulo")
    private String telefone;

    @NotNull(message = "O campo logadouro não pode ser nulo")
    private String logradouro;

    @NotNull(message = "O campo bairro não pode ser nulo")
    private String bairro;

    @NotNull(message = "O campo cidade não pode ser nulo")
    private String cidade;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Servico> servicos;

    public Pessoa() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoPessoaEnum getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(TipoPessoaEnum tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servico> servicos) {
        this.servicos = servicos;
    }
}
